FactoryGirl.define do
  factory :badge_group do
    name Faker::Name.unique.first_name
  end
end
