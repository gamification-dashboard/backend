FactoryGirl.define do
  factory :level do
    name { Faker::Name.unique.name }
    description { Faker::Lorem.sentence }
    start_range do |level|
      amount =  Level.count
      (amount == 0) ? 0 : amount + 1
    end
    end_range {start_range + 1}
  end
end
