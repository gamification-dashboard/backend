FactoryGirl.define do
  factory :activity_type do
    name { Faker::Name.unique.name }
    identifier { Faker::Name.unique.name }
    description { Faker::Lorem.sentence }
    points_definition 10
    association :source, strategy: :create
  end
end
