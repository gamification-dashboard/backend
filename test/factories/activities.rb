FactoryGirl.define do

  factory :activity do
    title { Faker::Name.unique.name }
    details { Faker::Lorem.sentence }
    association :activity_type, strategy: :create
    association :user, strategy: :create
  end

end
