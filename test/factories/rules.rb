FactoryGirl.define do
  factory :rule do
    name "MyString"
    amount 1
    begin_period "2017-10-30 11:26:34"
    end_period "2017-10-30 11:26:34"
    association :activity_type, strategy: :create
    association :badge, strategy: :create
  end
end
