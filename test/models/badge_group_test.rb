require 'test_helper'

class BadgeGroupTest < ActiveSupport::TestCase

  test "reject empty name" do
    item = BadgeGroup.new
    !item.valid?
    assert item.errors[:name].present?
    item.name = 'Some'
    !item.valid?
    assert !item.errors[:name].present?
  end

  test "should the name be unique" do
    name = 'unique name'
    some_badge_group = create(:badge_group, name: name )
    badge_group = BadgeGroup.new
    badge_group.name = name
    badge_group.valid?
    assert badge_group.errors.include?(:name)
  end

end
