require 'test_helper'

class SourceTest < ActiveSupport::TestCase

  test "reject empty name" do
    item = Source.new
    !item.valid?
    assert item.errors[:name].present?
    item.name = 'Some'
    !item.valid?
    assert !item.errors[:name].present?
  end

  test "should the name be unique" do
    name = 'unique name'
    some_source = create(:source, name: name )
    source = Source.new
    source.name = name
    source.valid?
    assert source.errors.include?(:name)
  end

end
