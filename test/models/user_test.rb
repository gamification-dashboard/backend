require 'test_helper'

class UserTest < ActiveSupport::TestCase

   test "the user has badges" do
     user = create(:user)
     badge = create(:badge) 
     user.badges<< badge
     user.reload

     assert_equal 1, user.badges.count
   end

   test "the user has no repeated badges" do
     user = create(:user)
     badge = create(:badge) 
     user.badges<< badge
     assert_raise ActiveRecord::RecordNotUnique do
       user.badges<< badge
     end
   end

  test "points be the sum of activity points" do
    user = create(:user)
    create(:activity, user: user, points_of_action: 10)
    create(:activity, user: user, points_of_action: 15)
    create(:activity, user: user, points_of_action: 25)
    assert_equal 50, user.points
  end

  test "user level is the first level if user has no points" do
    level = create(:level, start_range: 0, end_range: 10)
    create(:level, start_range: 11, end_range: 20)
    create(:level, start_range: 21, end_range: 30)
    user = create(:user)
    assert_equal level, user.level
  end

  test "set user level according his points" do
    user = create(:user)
    create(:level, start_range: 0, end_range: 10)
    level = create(:level, start_range: 11, end_range: 20)
    create(:level, start_range: 21, end_range: 30)
    create(:activity, user: user, points_of_action: 15)
    user.save
    assert_equal level, user.level
  end

end
