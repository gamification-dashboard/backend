require 'test_helper'

class LevelTest < ActiveSupport::TestCase

  test "level position be defined in order of creation" do
    level = create(:level)
    assert_equal 1, level.position
    level = create(:level)
    assert_equal 2, level.position
  end

  test "the first start range is 0" do
    any_value = 30
    assert_equal 0, Level.count
    level = build(:level, start_range: any_value)
    level.valid?
    assert level.errors[:start_range].present?
  end

  test "next start_range level be the end_range plus one of the last created level" do
    last_end_range = 40
    create(:level, end_range: last_end_range)
    some_start_range = 30
    level = build(:level, start_range: some_start_range)
    level.valid?
    assert level.errors[:start_range].present?

    level.start_range = last_end_range + 1
    level.valid?
    assert !level.errors[:start_range].present?
  end

  test "start_range not be a negative number" do
    negative_start_range = -4
    level = build(:level, start_range: negative_start_range)
    level.valid?
    assert level.errors[:start_range].present?
    level.start_range = 0
    level.valid?
    assert !level.errors[:start_range].present?
  end

  test "end_range be greater than start_range" do
    create(:level, start_range: 0, end_range: 40)
    start_range = 41
    end_range = 39
    level = build(:level, start_range: start_range, end_range: end_range)
    level.valid?
    assert level.errors[:end_range].present?
    level.end_range = start_range + 2
    level.valid?
    assert !level.errors[:end_range].present?
  end

  test "last_level scope return the level of the last position" do
    create(:level, start_range: 0, end_range: 1)
    create(:level, start_range: 2, end_range: 3)
    level = create(:level, start_range: 4, end_range: 5)
    create(:level, start_range: 6, end_range: 7)
    level.position = 4
    level.save
    assert_equal level, Level.last_level
  end

  test "level_of_points return the current user level with amount of points in the middle of level" do
    create(:level, start_range: 0, end_range: 10)
    create(:level, start_range: 11, end_range: 20)
    level = create(:level, start_range: 21, end_range: 30)
    create(:level, start_range: 31, end_range: 40)
    assert_equal level, Level.level_of_points(24)
  end

  test "level_of_points return the current user level with amount of points in the inferior limit of level" do
    create(:level, start_range: 0, end_range: 10)
    create(:level, start_range: 11, end_range: 20)
    level = create(:level, start_range: 21, end_range: 30)
    create(:level, start_range: 31, end_range: 40)
    assert_equal level, Level.level_of_points(21)
  end

  test "level_of_points return the current user level with amount of points in the upper limit of level" do
    create(:level, start_range: 0, end_range: 10)
    create(:level, start_range: 11, end_range: 20)
    level = create(:level, start_range: 21, end_range: 30)
    create(:level, start_range: 31, end_range: 40)
    assert_equal level, Level.level_of_points(30)
  end

  test "level_of_points return the first level is nil is passed as parameter" do
    level = create(:level, start_range: 0, end_range: 10)
    create(:level, start_range: 11, end_range: 20)
    create(:level, start_range: 21, end_range: 30)
    create(:level, start_range: 31, end_range: 40)
    assert_equal level, Level.level_of_points(nil)
  end

  test "level_of_points return the first level is 0 is passed as parameter" do
    level = create(:level, start_range: 0, end_range: 10)
    create(:level, start_range: 11, end_range: 20)
    create(:level, start_range: 21, end_range: 30)
    create(:level, start_range: 31, end_range: 40)
    assert_equal level, Level.level_of_points(0)
  end

end
