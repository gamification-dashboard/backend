namespace :deploy do

  desc 'Updates shared/config/*.yml files with the proper ones for environment'
  task :upload_shared_config_files do
    config_files = []
    base_local_path = 'config/deploy/config/'
    run_locally do
      Dir.glob("#{base_local_path}**/*") do |file_name|
        config_files.push(file_name.gsub(base_local_path, '')) unless File.directory?(file_name)
      end
    end


    on roles(:all) do
      config_path = File.join shared_path, "config"
#      execute "mkdir -p #{config_path}"
      
      config_files.each do |file_name|
        remote_file_name = "#{config_path}/#{file_name}"
         local_path = File.join(base_local_path, file_name)
#puts "LOCAL PATH #{local_path} para #{remote_file_name}"
         directory =  remote_file_name.gsub(File.basename(remote_file_name), '')
#puts directory.inspect
         execute "mkdir -p #{directory}"
         upload! local_path, remote_file_name
         info "Upload new #{local_path} -> #{remote_file_name}"
      end
    end

  end
  before 'deploy:check:linked_dirs', 'deploy:upload_shared_config_files'

  desc 'Updates server config files'
  task :update_server_config do
    config_files = []
    base_local_path = 'config/deploy/config/'
    run_locally do
      Dir.glob("#{base_local_path}**/*") do |file_name|
        config_files.push(file_name.gsub(base_local_path, '')) unless File.directory?(file_name)
      end
    end
puts 'wwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwwww'
    on roles(:all) do
#puts config_files.inspect
#puts  execute("Dir.glob("#{shared_path}/*")).inspect
      config_files.map do |file_name|
        file_shared_path = "#{shared_path}/config/#{file_name}"
puts file_name.inspect
puts file_shared_path.inspect
        execute("ln -fs #{file_shared_path} /#{file_name}")
#       config_files.push(file_name.gsub(base_local_path, '')) unless File.directory?(file_name)
      end
    end
#    execute "ls -la #{shared_path}"
  end
#  after 'deploy:upload_shared_config_files', 'deploy:update_server_config'

end
