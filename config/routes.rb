Rails.application.routes.draw do

  scope :api do
#    resources :rules
    resources :ranking do 
      collection do
        get :users
      end
    end

    resources :users
    resources :levels
    resources :sources
    resources :action_types
    resources :activities
    resources :badges do
      collection do
        get :my
      end
    end
    resources :categories

    mount_devise_token_auth_for 'User', at: 'auth', controllers: {
      sessions:  'overrides/sessions'
    }
  end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
