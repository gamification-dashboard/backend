# README



[![build status](https://gitlab.com/gamification-dashboard/backend/badges/master/build.svg)](https://git.serpro/gamification/engine/commits/master)
[![coverage report](https://gitlab.com/gamification-dashboard/backend/badges/master/coverage.svg)](https://git.serpro/gamification/engine/commits/master)

* The project

API do frontend do projeto https://git.serpro/gamification/personal-dashboard

* Generate Diagrams

rake diagram:all

* Generate ERD diagram

bundle exec erd
