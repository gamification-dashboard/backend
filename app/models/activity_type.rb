class ActivityType < ApplicationRecord

  belongs_to :source
  has_many :acitivities
  has_many :rules
  belongs_to :category, required: false

  validates :name, presence: true
  validates :identifier, presence: true
  
  validates_uniqueness_of :name, scope: :source_id

end
