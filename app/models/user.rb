class User < ApplicationRecord
  # Include default devise modules.
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable,
          :confirmable, :omniauthable
  include DeviseTokenAuth::Concerns::User

  has_and_belongs_to_many :badges
  has_many :activities
  has_many :votes, :dependent => :destroy
  belongs_to :level, required: false

  before_validation :check_level_and_udpate

#  def points
#    self.activities.sum(:points_of_action)
#  end

  def check_level_and_udpate
    level = Level.level_of_points(self.points)
    self.level = level.is_a?(Level) ? level : level.last
  end

end
