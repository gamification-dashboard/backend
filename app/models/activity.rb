class Activity < ApplicationRecord

  belongs_to :activity_type
  belongs_to :user
  has_many :rules, through: :activity_type

  validates :title, presence: true

  before_validation :update_points
  after_create :update_user_points
  after_create :check_badge_creation

  delegate :points_definition, to: :activity_type, allow_nil: true

  scope :by_user, -> user {
    where(user: user)
  }

  scope :by_type, -> type {
    where(activity_type: type)
  }

  private

   def update_points
     self.points_of_action ||= self.points_definition
   end

   def update_user_points
     self.user.increment!(:points, self.points_of_action)
   end

   def check_badge_creation
     self.rules.map do |rule|
       amount_of_activities = self.user.activities.by_type(self.activity_type).count
       if(amount_of_activities == rule.amount)
         self.user.badges<< rule.badge
       end
     end
   end


end
