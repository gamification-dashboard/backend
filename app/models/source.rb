class Source < ApplicationRecord
  validates :name, presence: true, uniqueness: true
  has_many :activity_types

end
