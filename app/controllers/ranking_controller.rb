class RankingController < ApplicationController

  def users
    page = params[:page] || 1
    per_page = params[:per_page] || 10

    @users = User.all.order(points: :desc).page(page).per(per_page)

    render json: @users
  end 

end
