class BadgesController < ApplicationController
#  before_action :set_badge, only: [:show, :update, :destroy]
  before_action :authenticate_user!

  # GET /badges
  def index
    @badges = Badge.page(params[:page]).per(params[:per_page])

    render json: @badges
  end

  def my
    # FIXME make this test
    # FIXME make this method return the badges grouped by badge group
    @badges = current_user.badges.order(:position)

    render json: @badges
  end

#  # GET /badges/1
#  def show
#    render json: @badge
#  end
#
#  # POST /badges
#  def create
#    @badge = Badge.new(badge_params)
#
#    if @badge.save
#      render json: @badge, status: :created, location: @badge
#    else
#      render json: @badge.errors, status: :unprocessable_entity
#    end
#  end
#
#  # PATCH/PUT /badges/1
#  def update
#    if @badge.update(badge_params)
#      render json: @badge
#    else
#      render json: @badge.errors, status: :unprocessable_entity
#    end
#  end
#
#  # DELETE /badges/1
#  def destroy
#    @badge.destroy
#  end
#
  private
#    # Use callbacks to share common setup or constraints between actions.
#    def set_badge
#      @badge = Badge.find(params[:id])
#    end
#
    # Only allow a trusted parameter "white list" through.
    def badge_params
      params.require(:badge).permit(:name)
    end
end
