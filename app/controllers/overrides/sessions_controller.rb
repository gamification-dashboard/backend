module Overrides
  class SessionsController < DeviseTokenAuth::SessionsController
    before_action :load_ldap_config

    alias_method :render_create_error_bad_credentials_orig, :render_create_error_bad_credentials

    def render_create_error_bad_credentials
      ldap = LdapAuthentication.new(@ldap_config)
      login = params[:email]
      password = params[:password]
      attrs = ldap.authenticate(login, password)
      # attrs is nil if the bind could not be defined
      user_login = get_login(attrs, ldap.attr_login, login) unless attrs.nil?
      if(user_login)
        @resource = User.find_or_initialize_by(login: user_login)

        if @resource.new_record?
          @resource.login = attrs[:uid].first
          @resource.email = attrs[:mail]
          @resource.name =  attrs[:fullname]
          @resource.password = password
          @resource.password_confirmation = password
          @resource.confirm
        end

        # create client id
        @client_id = SecureRandom.urlsafe_base64(nil, false)
        @token     = SecureRandom.urlsafe_base64(nil, false)

        @resource.tokens[@client_id] = {
          token: BCrypt::Password.create(@token),
          expiry: (Time.now + DeviseTokenAuth.token_lifespan).to_i
        }
        @resource.save!

        sign_in(:user, @resource, store: false, bypass: false)
        yield @resource if block_given?

        render_create_success
      else
        render_create_error_bad_credentials_orig
      end
    end

    def get_login(attrs, attr_login, login)
      user_login = Array.wrap(Array.wrap(attr_login).map{|v|attrs[v.to_sym]})
      user_login.empty? ? login : user_login.first
    end

    def load_ldap_config
      @ldap_config ||= YAML.load_file(File.join(Rails.root, 'config', 'ldap.yml'))[Rails.env]
      @ldap_config
    end

    # override devise serialization to use project user serialization
    def render_create_success
      render json: { data: ActiveModelSerializers::SerializableResource.new(@resource).as_json }
    end

  end
end
