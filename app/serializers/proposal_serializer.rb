class ProposalSerializer < ActiveModel::Serializer

  attributes :id, :title, :description, :contribution, :authors, :votes_count, :driver_id, :voting_users_ids, :user_id

  belongs_to :driver

end
