class StrategicObjectiveSerializer < ActiveModel::Serializer

  attributes :id, :title, :description
  has_many :drivers

end
