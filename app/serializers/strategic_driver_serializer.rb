class StrategicDriverSerializer < ActiveModel::Serializer
  attributes :id, :title, :description, :objective_id
end
