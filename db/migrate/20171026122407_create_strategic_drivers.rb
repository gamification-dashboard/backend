class CreateStrategicDrivers < ActiveRecord::Migration[5.0]
  def change
    create_table :strategic_drivers do |t|
      t.string :title
      t.text :description
      t.belongs_to :strategic_objective

      t.timestamps
    end
  end
end
