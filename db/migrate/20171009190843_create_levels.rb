class CreateLevels < ActiveRecord::Migration[5.0]
  def change
    create_table :levels do |t|
      t.string :name
      t.text :description
      t.integer :start_range, :end_range
      t.integer :position

      t.timestamps
    end
  end
end
