class CreateActivities < ActiveRecord::Migration[5.0]
  def change
    create_table :activities do |t|
      t.string :title
      t.text :details
      t.integer :points_of_action
      t.belongs_to :activity_type
      t.belongs_to :user

      t.timestamps
    end
  end
end
