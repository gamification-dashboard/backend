class CreateRules < ActiveRecord::Migration[5.0]
  def change
    create_table :rules do |t|
      t.string :name
      t.integer :amount
      t.datetime :begin_period
      t.datetime :end_period

      t.belongs_to :activity_type
      t.belongs_to :badge

      t.timestamps
    end
  end
end
