# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

require_relative './seeds/helper'

puts 'Povoando os dados...'

User.destroy_all
ActivityType.destroy_all
Activity.destroy_all
Level.destroy_all

Level.create!(start_range: 0, end_range: 50, name: 'Obi-Wan Kenobi (Star Wars)', description: 'Que a Força esteja com você')
Level.create!(start_range: 51, end_range: 200, name: 'Leônidas de Esparta (300)', description: 'This is SERPROOOOO…')
Level.create!(start_range: 201, end_range: 400, name: 'Magnus Carlsen (Xadrez)', description: 'Sem o elemento de prazer, não vale a pena tentar se destacar em nada.')
Level.create!(start_range: 401, end_range: 800, name: 'Steve Jobs (TI)', description: 'Design não é apenas como se parece. Design é como funciona')
Level.create!(start_range: 801, end_range: 1600, name: 'Sherlock Holmes', description: 'Agora é xeque e, por acaso, mate')
Level.create!(start_range: 1601, end_range: 3200, name: 'Mark Zuckerberg (TI)', description: 'Quando damos a toda a gente uma voz e damos a toda a gente poder, o sistema acaba por se tornar um lugar realmente melhor')
Level.create!(start_range: 3201, end_range: 6400, name: 'Han Solo (Star Wars)', description: 'É quando a diversão começa…')
Level.create!(start_range: 6401, end_range: 12800, name: 'Linus Torvalds (TI)', description: 'Sério, eu não tenho por meta destruir a Microsoft. Este será um efeito colateral completamente involuntário')
Level.create!(start_range: 12801, end_range: 25600, name: 'Batman (Bruce Wayne)', description: 'Não é quem eu sou por dentro e sim, o que eu faço é que me define')
Level.create!(start_range: 25601, end_range: 999999, name: 'Gandhi', description: 'Temos de nos tornar na mudança que queremos ver. ')

admin = User.create(:email => 'admin@localhost.com', :password => '12345678', :password_confirmation => '12345678', :name => 'admin')
admin.confirm
user = admin

Source.destroy_all
source = Source.create!(name: '#você.serpro')

BadgeGroup.destroy_all
Badge.destroy_all
badge_group = BadgeGroup.create!(:name => 'Entrada na Comunidade do Planejamento')
badge = Badge.create!(name: 'Entrada na Comunidade Ouro', :group => badge_group, picture: File.open(File.join(Rails.root,'db','seeds','assets','entrada_ouro.png'),'rb'))
user.badges<< badge
badge = Badge.create!(name: 'Entrada na Comunidade Prata', :group => badge_group, picture: File.open(File.join(Rails.root,'db','seeds','assets','entrada_prata.png'),'rb'))
user.badges<< badge
badge = Badge.create!(name: 'Entrada na Comunidade Bronze', :group => badge_group, picture: File.open(File.join(Rails.root,'db','seeds','assets','entrada_bronze.png'),'rb'))
create_user.badges<< badge
user.badges<< badge

badge_group = BadgeGroup.create!(:name => 'Participante Estratégico')
badge = Badge.create!(name: 'Participante Estrategico Ouro', :group => badge_group, picture: File.open(File.join(Rails.root,'db','seeds','assets','participante_ouro.png'),'rb'))
user.badges<< badge
badge = Badge.create!(name: 'Participante Estrategico Ouro', :group => badge_group, picture: File.open(File.join(Rails.root,'db','seeds','assets','participante_prata.png'),'rb'))
user.badges<< badge
badge = Badge.create!(name: 'Participante Estrategico Ouro', :group => badge_group, picture: File.open(File.join(Rails.root,'db','seeds','assets','participante_bronze.png'),'rb'))
user.badges<< badge
create_user.badges<< badge

badge_group = BadgeGroup.create!(:name => 'Entrar no Jogo do Planejamento')
badge = Badge.create!(name: 'Gamer Ouro', :group => badge_group, picture: File.open(File.join(Rails.root,'db','seeds','assets','gamer_ouro.png'),'rb'))
badge = Badge.create!(name: 'Gamer Prata', :group => badge_group, picture: File.open(File.join(Rails.root,'db','seeds','assets','gamer_prata.png'),'rb'))
user.badges<< badge
badge = Badge.create!(name: 'Gamer Bronze', :group => badge_group, picture: File.open(File.join(Rails.root,'db','seeds','assets','gamer_bronze.png'),'rb'))
create_user.badges<< badge

#Badge.create!(name: 'Antenado no PE Prata', picture: File.open(File.join(Rails.root,'db','seeds','assets','antenado.png'),'rb'))
#Badge.create!(name: 'Antenado no PE Bronze', picture: File.open(File.join(Rails.root,'db','seeds','assets','antenado.png'),'rb'))
#Badge.create!(name: 'Expectador Estrategico Ouro', picture: File.open(File.join(Rails.root,'db','seeds','assets','expectador.png'),'rb'))
#Badge.create!(name: 'Expectador Estrategico Prata', picture: File.open(File.join(Rails.root,'db','seeds','assets','expectador.png'),'rb'))
#Badge.create!(name: 'Expectador Estrategico Bronze', picture: File.open(File.join(Rails.root,'db','seeds','assets','expectador.png'),'rb'))
#Badge.create!(name: 'Marketeiro Estrategico Ouro', picture: File.open(File.join(Rails.root,'db','seeds','assets','marqueteiro.png'),'rb'))
#Badge.create!(name: 'Marketeiro Estrategico Prata', picture: File.open(File.join(Rails.root,'db','seeds','assets','marqueteiro.png'),'rb'))
#Badge.create!(name: 'Marketeiro Estrategico Bronze', picture: File.open(File.join(Rails.root,'db','seeds','assets','marqueteiro.png'),'rb'))


Category.destroy_all
category_economy = Category.create!(name: 'Econômico Financeiro')
category_clientes = Category.create!(name: 'Clientes')
category_process = Category.create!(name: 'Processos Internos')
category_people = Category.create!(name: 'Pessoas e Aprendizado')
category_tec = Category.create!(name: 'Tecnlogias')


activity_type = ActivityType.create!(name: 'Entrar no Jogo do Planejamento', description: 'Envolvimento no início de lançamento na gamificação demonstrando engajamento', points_definition: 50, source: source, category: category_process, identifier: 'enter_game' )
user = admin
Activity.create!(title: "#{user.name} entrou na comunidade do planejamento estratégico", activity_type: activity_type, user: user)
user = create_user
Activity.create!(title: "#{user.name} entrou na comunidade do planejamento estratégico", activity_type: activity_type, user: user)
#user = create_user
#Activity.create!(title: "#{user.name} entrou na comunidade do planejamento estratégico", activity_type: activity_type, user: user)
#user = create_user
#Activity.create!(title: "#{user.name} entrou na comunidade do planejamento estratégico", activity_type: activity_type, user: user)
#user = create_user
#Activity.create!(title: "#{user.name} entrou na comunidade do planejamento estratégicoo", activity_type: activity_type, user: user)


activity_type = ActivityType.create!(name: 'Interargir na comunidade do PE', description: 'Participar da comunidade do PE no Você.Serpro', points_definition: 200, source: source, category: category_process)
user= admin
Activity.create!(title: "#{user.name} comentou no artigo 'Resultado da pesquisa realizada com o corpo funcional - Planejamento Estratégico' da comunidade planejamento.serpro", details: 'Link do artigo: http://planejamento.serpro/noticias-do-planejamento/faca-parte-do-planejamento-estrategico-ciclo-2018', activity_type: activity_type, user: user)
Activity.create!(title: "#{user.name} comentou no artigo 'Serpro reafirma compromisso com estratégia para o crescimento econômico' da comunidade planejamento.serpro", details: 'Link do artigo: http://planejamento.serpro/noticias-do-planejamento/faca-parte-do-planejamento-estrategico-ciclo-2018', activity_type: activity_type, user: user)
#Activity.create!(title: "#{user.name} comentou no artigo 'Primeiro dia do Cenários Estratégicos: Inovação em Tecnologia' da comunidade planejamento.serpro", details: 'Link do artigo: http://planejamento.serpro/noticias-do-planejamento/faca-parte-do-planejamento-estrategico-ciclo-2018', activity_type: activity_type, user: user)

user = get_user
Activity.create!(title: "#{user.name} comentou no artigo 'Segundo dia do Cenários Estratégicos: Estratégia e Mercado' da comunidade planejamento.serpro", details: 'Link do artigo: http://planejamento.serpro/noticias-do-planejamento/faca-parte-do-planejamento-estrategico-ciclo-2018', activity_type: activity_type, user: user)
#user = get_user
#Activity.create!(title: "#{user.name} comentou no artigo 'Faça parte do Planejamento Estratégico Ciclo 2018' da comunidade http://planejamento.serpro", details: 'Link do artigo: http://planejamento.serpro/noticias-do-planejamento/faca-parte-do-planejamento-estrategico-ciclo-2018', activity_type: activity_type, user: user)


activity_type = ActivityType.create!(name: 'Entrar no Meu Serpro', description: 'Envolvimento no lançamento do game', points_definition: 50, source: source, category: category_process)
user = admin
Activity.create!(title: "#{user.name} entrou no jogo Meu Serpro", activity_type: activity_type, user: user)
user = create_user
Activity.create!(title: "#{user.name} entrou no jogo Meu Serpro", activity_type: activity_type, user: user)
user = create_user
Activity.create!(title: "#{user.name} entrou no jogo Meu Serpro", activity_type: activity_type, user: user)
#user = create_user
#Activity.create!(title: "#{user.name} entrou no jogo Meu Serpro", activity_type: activity_type, user: user)
#user = create_user
#Activity.create!(title: "#{user.name} entrou no jogo Meu Serpro", activity_type: activity_type, user: user)



activity_type = ActivityType.create!(name: 'Curti Artigos do Planejamento', description: 'Curti artigo do você.serpro', points_definition: 15, source: source, category: category_process)
Activity.create!(title: "#{user.name} curtiu o artigo 'Resultado da pesquisa realizada com o corpo funcional - Planejamento Estratégico' da comunidade planejamento.serpro", details: 'Link do artigo: http://planejamento.serpro/noticias-do-planejamento/faca-parte-do-planejamento-estrategico-ciclo-2018', activity_type: activity_type, user: user)
Activity.create!(title: "#{user.name} curtiu o artigo 'Primeiro dia do Cenários Estratégicos: Inovação em Tecnologia' da comunidade planejamento.serpro", details: 'Link do artigo: http://planejamento.serpro/noticias-do-planejamento/faca-parte-do-planejamento-estrategico-ciclo-2018', activity_type: activity_type, user: user)

user = create_user
Activity.create!(title: "#{user.name} curtiu o artigo 'Segundo dia do Cenários Estratégicos: Estratégia e Mercado' da comunidade planejamento.serpro", details: 'Link do artigo: http://planejamento.serpro/noticias-do-planejamento/faca-parte-do-planejamento-estrategico-ciclo-2018', activity_type: activity_type, user: user)
Activity.create!(title: "#{user.name} curtiu o artigo 'Faça parte do Planejamento Estratégico Ciclo 2018' da comunidade http://planejamento.serpro", details: 'Link do artigo: http://planejamento.serpro/noticias-do-planejamento/faca-parte-do-planejamento-estrategico-ciclo-2018', activity_type: activity_type, user: user)



activity_type_2 = ActivityType.create!(name: 'Visualizar Vídeo do Planejamento', description: 'Visualizar vídeo do Planejamento', points_definition: 10, source: source, category: category_process)
activity_type_ = ActivityType.create!(name: 'Envolvimento no Game', description: 'Ação de entrar no game', points_definition: 50, source: source, category: category_process)
#activity_type_ = ActivityType.create!(name: '', description: '', points_definition: 10, source: source, category: category_process)

StrategicObjective.destroy_all
StrategicDriver.destroy_all
Proposal.destroy_all


objective = StrategicObjective.create!(:title => 'OE3 - Atender proativamente as necessidades dos clientes para elevar o nível de satisfação a pelo menos 80 % até 2022')
driver = StrategicDriver.create!(:title => 'D3.1 - Alcançar nível de satisfação com o atendimento ao cliente de 85% em 2017', :objective => objective)
#Proposal.create(:title => '', :description => '', :status => Proposal::ACTIVE, :driver => driver )
proposals_count = 0
1.upto(30).map do
  proposals_count += 1
  Proposal.create!(:title => "proposta #{proposals_count}", :description => "desrição proposta #{proposals_count}", :status => Proposal::ACTIVE, :driver => driver)
end
driver = StrategicDriver.create!(:title => 'D3.2 - Alcançar nível de satisfação com a qualidade dos serviços de 73% em 2017', :objective => objective)
1.upto(30).map do
  proposals_count += 1
  Proposal.create!(:title => "proposta #{proposals_count}", :description => "desrição proposta #{proposals_count}", :status => Proposal::ACTIVE, :driver => driver )
end
driver = StrategicDriver.create!(:title => 'D3.3 - Alcançar nível de satisfação com o prazo de entrega de serviços de 64% em 2017', :objective => objective)
1.upto(30).map do
  proposals_count += 1
  Proposal.create!(:title => "proposta #{proposals_count}", :description => "desrição proposta #{proposals_count}", :status => Proposal::ACTIVE, :driver => driver )
end

objective = StrategicObjective.create!(:title => 'OE2 - Atingir o faturamento anual de R$ 4,17 bi em 2022')

driver = StrategicDriver.create!(:title => 'D2.1 - Atingir o faturamento anual de R$ 107 mi na linha de negócios Serviços de informação em 2017', :objective => objective)
1.upto(30).map do
  proposals_count += 1
  Proposal.create!(:title => "proposta #{proposals_count}", :description => "desrição proposta #{proposals_count}", :status => Proposal::ACTIVE, :driver => driver )
end
driver = StrategicDriver.create!(:title => 'D2.2 - Atingir o faturamento anual de R$ 124 mi na linha de negócios Serviços em Nuvem em 2017', :objective => objective)
1.upto(30).map do
  proposals_count += 1
  Proposal.create!(:title => "proposta #{proposals_count}", :description => "desrição proposta #{proposals_count}", :status => Proposal::ACTIVE, :driver => driver )
end


